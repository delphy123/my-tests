<?php
include_once('DBresultreg.class.php');
//include_once('DB.class.php');

class Login {

    public $setAction = "";
    public $message = "";
    public $class = "";
    function __construct(){

        if(isset($_REQUEST['setAction'])){
            $this->setAction = $_REQUEST['setAction'];
        }
        
        $ObjDBresultreg = new DBresultreg();
        switch($this->setAction){
            
            case "registerUser": 
                if($ObjDBresultreg->checkUserRegister($_POST)){

                    $this->messages = "Valid registration";
                    $this->class = "alert-success";
                } 
                else{
                    $this->message = "";
                    $this->class = "alert-danger";
                } 
                $this->setAction ="";  
                break;
        }
    }

}

