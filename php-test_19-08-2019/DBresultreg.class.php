<?php
require_once __DIR__ . '/regconfig.php';

class DBresultreg
{

    public $connection;

    function __construct()
    {

        $this->connection = mysqli_connect(HOST, DB_USERNAME, DB_PASSWORD, DATABASE);
    }

    function __destruct()
    {
        mysqli_close($this->connection);
    }


    /** 
     * Create or return a connection to the MySQL server.
     */

    public function getConnection()
    {
        return $this->connection;
    }

    /*  
        Function to check user is valid or not
        @param mixed $params
        @return boolean 
    */
    
    public function checkUserRegister($parameters)
    {   
        $Name = $this->cleanInput($parameters['Name']);
        $Email = $this->cleanInput($parameters['Email']);
        $Phonenumber = $this->cleanInput($parameters['Phonenumber']);
       
       /*  $query = "INSERT INTO userss(name, email, phonenumber) 
        VALUES('$Name', '$Email','$Phonenumber')";
        print $query; */
        $query = "INSERT INTO usertable(name, email, phonenumber) 
        VALUES('$Name', '$Email',$Phonenumber)";
        
        
        /* //ascending and descending 
        //$sql = "SELECT * FROM userss ORDER BY Name";
        //$sqli = "SELECT * FROM userss ORDER BY Email desc";
        $sqli = "SELECT * FROM userss ORDER BY Name ASC LIMIT 20";
        $sql = "SELECT * FROM userss ORDER BY Name DESC LIMIT 20";
        $sqlli = "SELECT NAME FROM userss WHERE NAME = 'molly'";*/
        /* $del = "DELETE FROM userss WHERE Name = neena";
        print $del; 
          */
       
        
        $result = mysqli_query($this->connection, $query);
        
        if(isset($result->num_rows)&&($result->num_rows > 0)) {
            
            return true;
        }
        else {
            return false;
        }
    }

    function cleanInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    

}

